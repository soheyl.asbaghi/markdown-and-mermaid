<!-- Headings -->

# Heading 1

## Heading 2

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

<!-- Italics -->

_This text_ is italic

_This text_ is italic

<!-- Strong -->

**This text** is italic

**This text** is italic

<!-- Strikethrough -->

~~This text~~ is strikethrough

<!-- Horizontal Rule -->

---

---

<!-- Blockquote -->

> This is a quote

<!-- Links -->

[Goole.com](http://www.google.com)

[Gitlab](https://gitlab.com/ 'Gitlab')

<!-- UL -->

- Item 1
- Item 2
- Item 3
  - Nested Item 1
  - Nested Item 2

<!-- OL -->

1. Item 1
1. Item 2
1. Item 3

<!-- Inline Code Block -->

`<p>This is a paragraph</p>`

<!-- Images -->

![Gitlab Logo](https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png)

<!-- Github Markdown -->

<!-- Code Blocks -->

```bash
  npm install

  npm start
```

```javascript
function add(num1, num2) {
	return num1 + num2;
}
```

```python
  def add(num1, num2):
    return num1 + num2
```

<!-- Tables -->

| Name           | Email            |
| -------------- | ---------------- |
| Soheyl Asbaghi | soheyl@gmail.com |
| Jane Doe       | jane@gmail.com   |

<!-- Task List -->

- [x] Task 1
- [x] Task 2
- [ ] Task 3

<!-- Mermaid -->

# Fabonacci series

```mermaid
graph TB
  A([Start])-->B[Declare Variable i, n, a, b, show]
  B --> C[Initialize, a = 0, b = 0 and show = 0]
  C --> D[/Input the number of term to be printed/]
  D --> E[/The Fabonacci series is: n\n\n<br> a b/]
  E --> F[i = 2]
  F --> G{i<=n<br> ?}
  G -- yes --> H[show = a + b <br> a = b <br> b = show]
  H --> K[/Print show/]
  K --> J([End])
  G --no --> J

```
